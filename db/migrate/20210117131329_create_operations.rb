class CreateOperations < ActiveRecord::Migration[6.0]
  def change
    execute <<-SQL
      CREATE TYPE operation_type AS ENUM ('transfer_from', 'transfer_to', 'sale');
    SQL
 
    create_table :operations do |t|
      t.references :storage, null: false, foreign_key: true
      t.references :product, null: false, foreign_key: true
      t.integer :quantity, presence: true
      t.column :operation_type, :operation_type, presence: true

      t.timestamps
    end
   end
end
