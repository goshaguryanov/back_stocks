STORAGES_QTY= 3
PRODUCTS_QTY= 30
MAX_PRODUCT_QTY = 100
TIMESTAMP = DateTime.now

Operation.delete_all
Storage.delete_all
Product.delete_all

Storage.insert_all Array.new(STORAGES_QTY) { 
  {
    title:  Faker::Company.name,
    adress:  Faker::Address.full_address,
    created_at: TIMESTAMP,
    updated_at: TIMESTAMP
  }
}

Product.insert_all Array.new(PRODUCTS_QTY) { 
  {
    title: Faker::Commerce.product_name,
    price: Faker::Commerce.price,
    created_at: TIMESTAMP,
    updated_at: TIMESTAMP
  }
}

storages = Storage.all
products = Product.all

# Для отоборажения пополнения количества товара на складе
# я использую операцию перемещения,
# но не указываю с какого склада перемещаю
Operation.insert_all products.map { |product|
  {
    operation_type: 'transfer_to',
    quantity: Random.rand(MAX_PRODUCT_QTY) + 1,
    product_id: product.id,
    storage_id: storages.sample.id,
    created_at: TIMESTAMP,
    updated_at: TIMESTAMP
  }
} 