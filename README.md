# README

 Rails-приложение - микросервис работы складов. Продукты могут находиться на складе, переезжать со склада на склад, реализовываться.

 Инструкция по использованию

* bundle install
* rake db:create
* rake db:migrate
* rake db:migrate RAILS_ENV=test
* rake db:seed

Запуск тестов

* bundle exec rspec

