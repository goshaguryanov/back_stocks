class Storage < ApplicationRecord
  validates :title, presence: true
  validates :adress, presence: true
  has_many :operations

  def self.all_with_balance    
    result connection.execute <<-SQL
      SELECT balance, storage_id, s.title, s.adress
      FROM (
        SELECT SUM(operations.quantity*p.price) AS BALANCE,
               storage_id
        FROM operations
        INNER JOIN products p on p.id = operations.product_id
        GROUP BY storage_id
      ) AS balances
      INNER JOIN storages s on s.id = balances.storage_id
    SQL
  end

  def balance
    @balance ||= operations.sales.joins(:product).
                  pluck(Arel.sql('products.price * operations.quantity AS sale_sum')).
                  reduce(0, :+).to_f
  end

  def quantity(product_id = nil)
    @quantity ||= transfered_to_quantity(product_id) - 
                  transfered_from_quantity(product_id) -
                  sold_quantity(product_id)
  end

  private 

  def transfered_to_quantity(product_id = nil)
    chain = operations.transfers_to
    unless product_id.nil?
      chain.where(product_id: product_id)
    end
    chain.sum(:quantity)
  end

  def transfered_from_quantity(product_id = nil)
    chain = operations.transfers_from
    unless product_id.nil?
      chain.where(product_id: product_id)
    end
    chain.sum(:quantity)
  end

  def sold_quantity(product_id = nil)
    chain = operations.sales
    unless product_id.nil?
      chain.where(product_id: product_id)
    end
    chain.sum(:quantity)
  end
end
