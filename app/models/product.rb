class Product < ApplicationRecord
  validates :title, presence: true
  validates :price, presence: true
  has_many :operations

  def quantity
    @quantity ||= transfers_to - transfers_from - sales
  end

  # params:
  # => :from
  # => :to
  # => :quantity
  def transfer(params)
    raise ArgumentError.new("#transfer need :from") unless params.has_key? :from
    raise ArgumentError.new("#transfer need :to") unless params.has_key? :to
    raise ArgumentError.new("#transfer need :quantity") unless params.has_key? :quantity
    transaction do
      Operation.create(product: self, storage: params[:from], quantity: params[:quantity], operation_type: 'transfer_from')
      Operation.create(product: self, storage: params[:to], quantity: params[:quantity], operation_type: 'transfer_to')
    end
  end

  # params:
  # => :storage
  # => :quantity
  def sell(params)
    Operation.create(product: self, storage: params[:storage], quantity: params[:quantity], operation_type: 'sale')
  end

  private

  def transfers_to
    operations.transfers_to.group(:storage_id).
    pluck(Arel.sql('sum(quantity) as qty')).
    reduce(0, :+)
  end

  def transfers_from
    operations.transfers_from.group(:storage_id).
    pluck(Arel.sql('sum(quantity) as qty')).
    reduce(0, :+)
  end

  def sales
    operations.sales.group(:storage_id).
    pluck(Arel.sql('sum(quantity) as qty')).
    reduce(0, :+)
  end
end
