class Operation < ApplicationRecord
  belongs_to :storage
  belongs_to :product
  validates :quantity, presence: true
  validates :operation_type, inclusion: { in: %w(transfer_from transfer_to sale), message: "%{value} is not a valid" }
  scope :sales, -> { where(operation_type: 'sale') }
  scope :transfers_to, -> { where(operation_type: 'transfer_to') }
  scope :transfers_from, -> { where(operation_type: 'transfer_from') }
  
  class << self
    def stat(params = {})
      raise ArgumentError.new(".stat need :start") unless params.has_key? :start
      raise ArgumentError.new(".stat need :end") unless params.has_key? :end
      @start = params[:start]
      @end = params[:end]
      connection.execute(sql_query).to_a.map do |hash|
        hash["earnings"] = hash["earnings"].to_f
        hash
      end
      # sql_query
    end

    def sql_query
      <<-SQL
      SELECT begin.storage_id, begin.product_id, begin.beginning_qty, ending.ending_qty, sold_qty, sold_qty*products.price as earnings
      FROM (
        #{beginning_period_qty}
      ) as begin
      RIGHT JOIN (
        #{end_period_qty}
      ) AS ending
      ON 
          begin.storage_id = ending.storage_id
      AND begin.product_id = ending.product_id
      RIGHT JOIN (
        #{sold_qty}
      ) AS sold
      ON begin.storage_id = sold.storage_id
      AND begin.product_id = sold.product_id
      
      FULL OUTER JOIN products
      ON begin.product_id = products.id      
      SQL
    end

    def beginning_period_qty
      <<-SQL
      SELECT storage_id, product_id, SUM(quantity) as beginning_qty
      FROM (
        #{positive_qty.where(arel_table[:created_at].lt(@start)).to_sql}
        UNION
        #{negative_qty.where(arel_table[:created_at].lt(@start)).to_sql}
      ) AS subquery
      GROUP BY storage_id, product_id
      SQL
    end

    def end_period_qty
      <<-SQL
      SELECT storage_id, product_id, SUM(quantity) as ending_qty
      FROM (
        #{positive_qty.where(arel_table[:created_at].lt(@end)).to_sql}
        UNION
        #{negative_qty.where(arel_table[:created_at].lt(@end)).to_sql}
      ) AS subquery
      GROUP BY storage_id, product_id
      SQL
    end

    def sold_qty
      arel_table.project(Arel.sql('storage_id, product_id, SUM(quantity) as sold_qty')).
      where(arel_table[:operation_type].eq('sale')).
      group(Arel.sql('storage_id, product_id')).to_sql
    end

    def method_name
      
    end

    def positive_qty
      arel_table.project(Arel.sql('storage_id, product_id, quantity')).
      where(arel_table[:operation_type].eq('transfer_to'))
    end

    def negative_qty
      arel_table.project(Arel.sql('storage_id, product_id, -quantity')).
      where(arel_table[:operation_type].in(['transfer_from', 'sale']))
    end

  end
end