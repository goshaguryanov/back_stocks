class ProductsController < ApplicationController
  rescue_from ActionController::ParameterMissing do |exception|
    render(json: { errors: @errors }, status: 500)        
  end

  rescue_from ActiveRecord::RecordNotFound do |exception|
    render(json: { errors: exception.message }, status: 500)        
  end

  # params:
  #   :product_id 
  #   :from_id 
  #   :to_id
  #   :quantity
  def transfer
    validate_params :product_id, :from_id, :to_id, :quantity
    product.transfer(from: from_storage, to: to_storage, quantity: quantity)
    render(json: { product_id: product.id, message: "Transfered from storage #{from_storage.id} to storage #{to_storage.id} in quantity #{quantity}" }, status: 200)        
  end

  # params:
  #   :product_id 
  #   :storage_id 
  #   :quantity
  def sell
    validate_params :product_id, :storage_id, :quantity
    product.sell(storage: storage, quantity: quantity)
    render(json: { product_id: product.id, message: "Sold out from storage #{storage.id} in quantity #{quantity}" }, status: 200)
  end

  private

  def validate_params(*required_keys)
    @errors = {}
    required_keys.each do |key|
      unless params[key].present?
        @errors[key] = I18n.t("errors.params.#{key}.presence")
      end
    end
    unless @errors.empty?
      raise ActionController::ParameterMissing.new 'ParameterMissing'
    end
  end

  def product
    @product ||= Product.find(params[:product_id])
  end

  def from_storage
    @from_storage ||= Storage.find(params[:from_id])
  end

  def to_storage
    @to_storage ||= Storage.find(params[:to_id])
  end

  def storage
    @storage ||= Storage.find(params[:storage_id])
  end

  def quantity
    params[:quantity]
  end

end
