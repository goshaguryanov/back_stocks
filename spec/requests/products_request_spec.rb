require 'rails_helper'

RSpec.describe "Products", type: :request do

  # params:
  #   :product_id 
  #   :from_id 
  #   :to_id
  #   :quantity
  describe "POST products/transfer" do
    describe "parameters" do
      context "there is not product_id parameter" do
        it "returns http error" do
          post "/products/transfer", params: {
            from_id: 1,
            to_id: 1,
            quantity: 1,
          }
          expect(response).to have_http_status(500)
        end

        it "returns json with error message" do
          post "/products/transfer", params: {
            from_id: 1,
            to_id: 1,
            quantity: 1,
          }
          expect(JSON.parse(response.body)['errors'].keys).to include('product_id')
        end
      end

      context "there is not from_id parameter" do
        it "returns http error" do
          post "/products/transfer", params: {
            product_id: 1,
            to_id: 1,
            quantity: 1,
          }
          expect(response).to have_http_status(500)
        end

        it "returns json with error message" do
          post "/products/transfer", params: {
            product_id: 1,
            to_id: 1,
            quantity: 1,
          }
          expect(JSON.parse(response.body)['errors'].keys).to include('from_id')
        end
      end

      context "there is not to_id parameter" do
        it "returns http error" do
          post "/products/transfer", params: {
            product_id: 1,
            from_id: 1,
            quantity: 1,
          }
          expect(response).to have_http_status(500)
        end

        it "returns json with error message" do
          post "/products/transfer", params: {
            product_id: 1,
            from_id: 1,
            quantity: 1,
          }
          expect(JSON.parse(response.body)['errors'].keys).to include('to_id')
        end
      end

      context "there is not quantity parameter" do
        it "returns http error" do
          post "/products/transfer", params: {
            product_id: 1,
            from_id: 1,
            to_id: 1,
          }
          expect(response).to have_http_status(500)
        end

        it "returns json with error message" do
          post "/products/transfer", params: {
            product_id: 1,
            from_id: 1,
            to_id: 1,
          }
          expect(JSON.parse(response.body)['errors'].keys).to include('quantity')
        end
      end
    end
  end

  # params:
  #   :product_id 
  #   :storage_id 
  #   :quantity
  describe "POST products/sell" do
   describe "parameters" do
      context "there is not product_id parameter" do
        it "returns http error" do
          post "/products/sell", params: {
            storage_id: 1,
            quantity: 1
          }
          expect(response).to have_http_status(500)
        end

        it "returns json with error message" do
          post "/products/sell", params: {
            storage_id: 1,
            quantity: 1
          }
          expect(JSON.parse(response.body)['errors'].keys).to include('product_id')
        end
      end

      context "there is not storage_id parameter" do
        it "returns http error" do
          post "/products/sell", params: {
            product_id: 1,
            quantity: 1
          }
          expect(response).to have_http_status(500)
        end

        it "returns json with error message" do
          post "/products/sell", params: {
            product_id: 1,
            quantity: 1
          }
          expect(JSON.parse(response.body)['errors'].keys).to include('storage_id')
        end
      end

      context "there is not quantity parameter" do
        it "returns http error" do
          post "/products/sell", params: {
            product_id: 1,
            storage_id: 1
          }
          expect(response).to have_http_status(500)
        end

        it "returns json with error message" do
          post "/products/sell", params: {
            product_id: 1,
            storage_id: 1
          }
          expect(JSON.parse(response.body)['errors'].keys).to include('quantity')
        end
      end
    end
   end

end
