require 'rails_helper'

RSpec.describe Product, type: :model do
  let(:params) { { title: 'title', price: 10 } }
  let(:storage_params) { { title: 'title', adress: 'adress' } }
  let(:product) { Product.create(params) }

  describe "validation" do
    it "should have title" do
      product = Product.new({price: 10})
      expect(product).not_to be_valid
    end

    it "should have price" do
      product = Product.new({title: "title"})
      expect(product).not_to be_valid
    end
  end

  describe "#quantity" do
    context "when there is not any Operation" do
      it "should return 0" do
        expect(product.quantity).to equal 0
      end
    end

    context "when there is one Operation {type: TRANSFER_TO, quantity: 1}" do
      before(:each) do
        storage = Storage.create(storage_params)
        Operation.create({
          product_id: product.id,
          storage_id: storage.id,
          operation_type: 'transfer_to',
          quantity: 1
        })
      end

      it "should return 1" do
        expect(product.quantity).to equal 1
      end
    end

    context "when there are two Operation {type: TRANSFER_TO, quantity: 1} with different storages" do
      before(:each) do
        storage1 = Storage.create(storage_params)
        storage2 = Storage.create(storage_params)
        Operation.create({
          product_id: product.id,
          storage_id: storage1.id,
          operation_type: 'transfer_to',
          quantity: 1
        })
        Operation.create({
          product_id: product.id,
          storage_id: storage2.id,
          operation_type: 'transfer_to',
          quantity: 1
        })
      end

      it "should return 1" do
        expect(product.quantity).to equal 2
      end
    end

    context "when there are Operation's {type: TRANSFER_TO, quantity: 2} and {type: TRANSFER_FROM, quantity: 1}" do
      before(:each) do
        storage = Storage.create(storage_params)
        Operation.create({
          product_id: product.id,
          storage_id: storage.id,
          operation_type: 'transfer_to',
          quantity: 2
        })
        Operation.create({
          product_id: product.id,
          storage_id: storage.id,
          operation_type: 'transfer_from',
          quantity: 1
        })
      end

      it "should return 1" do
        expect(product.quantity).to equal 1
      end
    end

    context "when there are Operation's {type: TRANSFER_TO, quantity: 3}, {type: SALE, quantity: 1} and {type: TRANSFER_FROM, quantity: 1}" do
      before(:each) do
        storage = Storage.create(storage_params)
        Operation.create({
          product_id: product.id,
          storage_id: storage.id,
          operation_type: 'transfer_to',
          quantity: 3
        })
        Operation.create({
          product_id: product.id,
          storage_id: storage.id,
          operation_type: 'sale',
          quantity: 1
        })
        Operation.create({
          product_id: product.id,
          storage_id: storage.id,
          operation_type: 'transfer_from',
          quantity: 1
        })
      end

      it "should return 1" do
        expect(product.quantity).to equal 1
      end
    end
  end

  describe "#transfer" do
    context "with wrong parameters" do
      it "should raise ArgumentError without :from" do
        expect {
          product.transfer(to: Storage.create(storage_params), quantity: 1)
        }.to raise_error(ArgumentError, "#transfer need :from")
      end
      it "should raise ArgumentError without :to" do
        expect {
          product.transfer(from: Storage.create(storage_params), quantity: 1)
        }.to raise_error(ArgumentError, "#transfer need :to")
      end
      it "should raise ArgumentError without :quantity" do
        expect {
          product.transfer(to: Storage.create(storage_params), from: Storage.create(storage_params))
        }.to raise_error(ArgumentError, "#transfer need :quantity")
      end
    end
    context "with valid parameters" do
      it "should change quanity" do
        from_storage = Storage.create(storage_params)
        to_storage = Storage.create(storage_params)
        Operation.create(product_id: product.id, storage_id: from_storage.id, quantity: 1, operation_type: 'transfer_to')
        product.transfer(from: from_storage, to: to_storage, quantity: 1)
        expect(to_storage.quantity).to eq(1)
      end
    end
  end
end
