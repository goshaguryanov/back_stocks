require 'rails_helper'

RSpec.describe Storage, type: :model do
  let(:params) { { title: 'title', adress: 'adress' } }
  let(:product_params) { { title: 'title', price: 10.0 } }
  let(:storage) { Storage.create(params) }

  describe "validation" do
    it "should have title" do
      storage = Storage.new({adress: 'adress'})
      expect(storage).not_to be_valid
    end

    it "should have adress" do
      storage = Storage.new({title: 'title'})
      expect(storage).not_to be_valid
    end
  end

  describe "#balance" do
    context "when there is not any Operation" do
      it "should return 0" do
        expect(storage.balance).to equal 0.0
      end
    end

    context "when there is one Operation {type: SALE, quantity: 1}" do
      let(:product) { Product.create(product_params) }
      
      before(:each) do
        Operation.create({
          product_id: product.id,
          storage_id: storage.id,
          operation_type: 'sale',
          quantity: 1
        })
      end

      it "should return sum of product.price * operation.quantity" do
        expect(storage.balance).to eq product.price
      end
    end
  end

  describe "#quantity" do
    context "when exist only Operation with {operation_type = transfer_to, quantity: 1}" do
      let(:product) { Product.create(product_params) }
      before(:each) do
        Operation.create({
          product_id: product.id,
          storage_id: storage.id,
          operation_type: 'transfer_to',
          quantity: 1
        })
      end

      it "should return sum of related Operation quantity" do
        expect(storage.quantity).to eq(1)
      end
    end

   context "when exist Operation with {operation_type = transfer_to, quantity: 2} and
                                      {operation_type = transfer_from, quantity: 1}" do
      let(:product) { Product.create(product_params) }
      before(:each) do
        Operation.create({
          product_id: product.id,
          storage_id: storage.id,
          operation_type: 'transfer_to',
          quantity: 2
        })
        Operation.create({
          product_id: product.id,
          storage_id: storage.id,
          operation_type: 'transfer_from',
          quantity: 1
        })
      end

      it "should return sum of related Operation quantity" do
        expect(storage.quantity).to eq(1)
      end
    end

    



    context "when exist Operation with {operation_type = transfer_to, quantity: 2} and 
                                       {operation_type = sale, quantity: 1}" do
      let(:product) { Product.create(product_params) }
      before(:each) do
        Operation.create({
          product_id: product.id,
          storage_id: storage.id,
          operation_type: 'transfer_to',
          quantity: 2
        })
        Operation.create({
          product_id: product.id,
          storage_id: storage.id,
          operation_type: 'sale',
          quantity: 1
        })
      end

      it "should return sum of related Operation quantity" do
        expect(storage.quantity).to eq(1)
      end
    end
  end
end
