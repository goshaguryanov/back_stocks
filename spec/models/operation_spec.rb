# TODO 
# Use factory_bot
require 'rails_helper'

RSpec.describe Operation, type: :model do
  describe "validation" do
    it "should have product_id" do
      operation = Operation.new({
        storage_id: 1,
        operation_type: 'transfer_to',
        quantity: 1
      })
      expect(operation).not_to be_valid
    end

    it "should have storage_id" do
      operation = Operation.new({
        product_id: 1,
        operation_type: 'transfer_to',
        quantity: 1
      })
      expect(operation).not_to be_valid
    end

    it "should have operation_type" do
      operation = Operation.new({
        storage_id: 1,
        product_id: 1,
        quantity: 1
      })
      expect(operation).not_to be_valid
    end

    it "should have valid operation_type" do
      operation = Operation.new({
        storage_id: 1,
        product_id: 1,
        quantity: 1,
        operation_type: "not_valid"
      })
      expect(operation).not_to be_valid
    end

    it "should have quantity" do
      operation = Operation.new({
        storage_id: 1,
        product_id: 1,
        operation_type: 'transfer_to'
      })
      expect(operation).not_to be_valid
    end
  end

  describe "scopes" do
    let(:product) { Product.create(title: 'title', price: 1) }
    let(:storage) { Storage.create(title: 'title', adress: 'adress') }
    after(:each) {
      Operation.delete_all
      Product.delete_all
      Storage.delete_all
    }

    it "should be able to choose operations with operation_type == `sale` " do
      operation = Operation.create({
        product_id: product.id,
        storage_id: storage.id,
        operation_type: 'sale',
        quantity: 1
      })
      expect(Operation.sales.first.id).to equal(operation.id)
    end

    it "should be able to choose operations with operation_type == `transfer_to` " do
      operation = Operation.create({
        product_id: product.id,
        storage_id: storage.id,
        operation_type: 'transfer_to',
        quantity: 1
      })
      expect(Operation.transfers_to.first.id).to equal(operation.id)
    end

    it "should be able to choose operations with operation_type == `transfer_from` " do
      operation = Operation.create({
        product_id: product.id,
        storage_id: storage.id,
        operation_type: 'transfer_from',
        quantity: 1
      })
      expect(Operation.transfers_from.first.id).to equal(operation.id)
    end
  end

  describe ".stat" do
    context "without valid params" do
      it "should raise ArgumentError without params[:start]" do
        expect {
          Operation.stat(end: DateTime.now)          
        }.to raise_error(ArgumentError, ".stat need :start")
      end

      it "should raise ArgumentError without params[:start]" do
        expect {
          Operation.stat(start: DateTime.now)          
        }.to raise_error(ArgumentError, ".stat need :end")
      end
    end

    context "with valid params" do
      let(:product_params) { { title: 'title', price: 10 } }
      let(:storage_params) { { title: 'title', adress: 'adress' } }
      let(:product) { Product.create(product_params) }
      let(:storage) { Storage.create(storage_params) }
      let(:operation_params) { { product_id: product.id, storage_id: storage.id }}

      it "should return quantity of products on storage at the beginning of the period" do
        Operation.create(operation_params.merge({
          quantity: 5,
          created_at: DateTime.now - 1.month - 1.day,
          operation_type: 'transfer_to'
        }))
        Operation.create(operation_params.merge({
          quantity: 1,
          created_at: DateTime.now - 1.month - 1.day,
          operation_type: 'transfer_from'
        }))
        Operation.create(operation_params.merge({
          quantity: 2 ,
          created_at: DateTime.now - 1.month - 1.day,
          operation_type: 'sale'
        }))
        stat = Operation.stat(start: DateTime.now - 1.month, end: DateTime.now)
        expect(stat.first["beginning_qty"]).to eq(2)
      end

      it "should return quantity of products on storage at the end of the period" do
        Operation.create(operation_params.merge({
          quantity: 5,
          created_at: DateTime.now - 1.month - 1.day,
          operation_type: 'transfer_to'
        }))
        Operation.create(operation_params.merge({
          quantity: 1,
          created_at: DateTime.now - 1.day,
          operation_type: 'transfer_from'
        }))
        Operation.create(operation_params.merge({
          quantity: 2 ,
          created_at: DateTime.now - 1.day,
          operation_type: 'sale'
        }))
        stat = Operation.stat(start: DateTime.now - 1.month, end: DateTime.now)
        expect(stat.first["ending_qty"]).to eq(2)
      end

      it "should return quantity of sold products on storage for the end of the period" do
        Operation.create(operation_params.merge({
          quantity: 5,
          created_at: DateTime.now - 1.month - 1.day,
          operation_type: 'transfer_to'
        }))
        Operation.create(operation_params.merge({
          quantity: 1,
          created_at: DateTime.now - 1.day,
          operation_type: 'transfer_from'
        }))
        Operation.create(operation_params.merge({
          quantity: 2,
          created_at: DateTime.now - 1.day,
          operation_type: 'sale'
        }))
        stat = Operation.stat(start: DateTime.now - 1.month, end: DateTime.now)
        expect(stat.first["sold_qty"]).to eq(2)
      end

      it "should return quantity of earnings for sold products" do
        Operation.create(operation_params.merge({
          quantity: 5,
          created_at: DateTime.now - 1.month - 1.day,
          operation_type: 'transfer_to'
        }))
        Operation.create(operation_params.merge({
          quantity: 1,
          created_at: DateTime.now - 1.day,
          operation_type: 'sale'
        }))
        stat = Operation.stat(start: DateTime.now - 1.month, end: DateTime.now)
        expect(stat.first["earnings"]).to eq(product.price)
      end
    end
  end
end
